# Automation Team Recruitment Test

## Introduction

This exercise is meant to test basic programming skills in Python.
It is written in **Python 3** so you will need an appropriate runtime
environment in order to complete it.

## The exercise

You will need to implement a function called `delete_old_versions()`
within the file `delete_old_versions.py`. This function simulates a
cleanup process which removes old software versions from some
repository while keeping the **N latest versions** where N is an
arbitrary positive integer.

### Arguments
The function should accept **two** positional arguments - **input**
and **n**.

The first argument is a **string** in the following format:

        {
          "versions": [
            {
              "dateCreated": "1975-04-18 08:50:46",
              "id": "6be15701-ca6e-40c9-b17c-21d4003f82de"
            },
            {
              "dateCreated": "1999-02-06 01:10:24",
              "id": "6431a8f9-6627-44a4-bbb7-ac57625d7429"
            },
            {
              "dateCreated": "1974-11-30 05:57:28",
              "id": "be3b5d75-48ec-4c10-80db-6ed53a6f24a7"
            }
          ]
        }

This argument represents a collection of software versions and may
contain an arbitrary number of elements under the `"versions"` key.
In the example shown above there are **3** elements. Each version has
a random ID and a timestamp representing its creation date.

The second argument is a **positive integer**, which can be any whole
number except zero and negative numbers. This argument represents
the number of versions **to keep**.

### Return Value

The function should return a **serialized JSON object** (which is
a string) representing the **remaining** versions after deleting the
**N oldest versions** where N is the second argument. For example,
running the function with the example above as the first argument and
with `2` as the second argument should return the following:

        {
          "versions": [
            {
              "dateCreated": "1975-04-18 08:50:46",
              "id": "6be15701-ca6e-40c9-b17c-21d4003f82de"
            },
            {
              "dateCreated": "1999-02-06 01:10:24",
              "id": "6431a8f9-6627-44a4-bbb7-ac57625d7429"
            }
          ]
        }

That's because we had to keep the **2** newest versions, therefore we
had to delete just the oldest version.

## Instructions

- The only file you should change is `delete_old_versions.py`. All
other files should remain unchanged.
- You don't need to install any Python modules to complete the
exercise, however you *may* do so if you can't complete the exercise
otherwise.
- You can assume the arguments are OK and you don't have to check
them.
- If the value in the second argument is larger than the number of
versions in the first argument, the function should return all the
versions which were provided.
- Try to make the solution **as efficient as possible** in terms of
CPU usage so that it can handle large inputs. If your solution is
inefficient, some of the tests will probably take a long time to
complete (more than a few seconds).
- You may use any resources you need in order to complete this task
(Google, books etc.), however please do it by yourself and make sure
you can explain every line in your solution and understand what it
does.

## Verifying Your Work

In order to verify your solution, run `python tests.py`. You should
get the following:

    Checking 'versions1.json'...
    Looks good.
    Checking 'versions2.json'...
    Looks good.
    Checking 'versions3.json'...
    Looks good.
    Checking 'versions4.json'...
    Looks good.
    All tests have passed. Nice job!

If you've got anything else, please review your solution and / or the
instructions.
